/*
 * FileName：MpAccount.java 
 * <p>
 * Copyright (c) 2017-2020, <a href="http://www.webcsn.com">hermit (794890569@qq.com)</a>.
 * <p>
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl-3.0.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.wxmp.wxapi.process;

import lombok.Data;

import java.io.Serializable;

/**
 * 微信公众号信息
 */
public class MpAccount implements Serializable {
    private static final long serialVersionUID = -6315146640254918207L;
    
    private String account;// 账号
    
    private String appid;// appid
    
    private String appsecret;// appsecret
    
    private String url;// 验证时用的url
    
    private String token;// token
    // ext
    
    private Integer msgCount;// 自动回复消息条数;默认是5条
    
    public Integer getMsgcount() {
        if (msgCount == null)
            msgCount = 5;// 默认5条
        return msgCount;
    }
    
    /**
     * 获取 account
     * @return 返回 account
     */
    public String getAccount() {
        return account;
    }
    
    /**
     * 设置 account
     * @param 对account进行赋值
     */
    public void setAccount(String account) {
        this.account = account;
    }
    
    /**
     * 获取 appid
     * @return 返回 appid
     */
    public String getAppid() {
        return appid;
    }
    
    /**
     * 设置 appid
     * @param 对appid进行赋值
     */
    public void setAppid(String appid) {
        this.appid = appid;
    }
    
    /**
     * 获取 appsecret
     * @return 返回 appsecret
     */
    public String getAppsecret() {
        return appsecret;
    }
    
    /**
     * 设置 appsecret
     * @param 对appsecret进行赋值
     */
    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }
    
    /**
     * 获取 url
     * @return 返回 url
     */
    public String getUrl() {
        return url;
    }
    
    /**
     * 设置 url
     * @param 对url进行赋值
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * 获取 token
     * @return 返回 token
     */
    public String getToken() {
        return token;
    }
    
    /**
     * 设置 token
     * @param 对token进行赋值
     */
    public void setToken(String token) {
        this.token = token;
    }
    
    /**
     * 获取 serialversionuid
     * @return 返回 serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    
    /**
     * 获取 msgCount
     * @return 返回 msgCount
     */
    public Integer getMsgCount() {
        return msgCount;
    }
    
    /**
     * 设置 msgCount
     * @param 对msgCount进行赋值
     */
    public void setMsgCount(Integer msgCount) {
        this.msgCount = msgCount;
    }
}
