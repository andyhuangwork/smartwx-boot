/*
 * FileName：MsgBase.java 
 * <p>
 * Copyright (c) 2017-2020, <a href="http://www.webcsn.com">hermit (794890569@qq.com)</a>.
 * <p>
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl-3.0.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.wxmp.wxcms.domain;

import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wxmp.core.page.Page;

/**
 *
 * @author hermit
 * @version 2.0
 * @date 2018-04-17 10:54:58
 */
public class MsgBase extends Page implements Serializable {
    private Long id;
    
    private String msgType;// 消息类型;
    
    private String inputCode;// 关注者发送的消息
    
    @JsonIgnore
    private String rule;// 规则，目前是 “相等”
    
    @JsonIgnore
    private Integer enable;// 是否可用
    
    @JsonIgnore
    private Integer readCount;// 消息阅读数
    
    @JsonIgnore
    private Integer favourCount;// 消息点赞数
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createTime;// 创建时间

    /**
     * 获取 id
     * @return 返回 id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置 id
     * @param 对id进行赋值
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取 msgType
     * @return 返回 msgType
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     * 设置 msgType
     * @param 对msgType进行赋值
     */
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     * 获取 inputCode
     * @return 返回 inputCode
     */
    public String getInputCode() {
        return inputCode;
    }

    /**
     * 设置 inputCode
     * @param 对inputCode进行赋值
     */
    public void setInputCode(String inputCode) {
        this.inputCode = inputCode;
    }

    /**
     * 获取 rule
     * @return 返回 rule
     */
    public String getRule() {
        return rule;
    }

    /**
     * 设置 rule
     * @param 对rule进行赋值
     */
    public void setRule(String rule) {
        this.rule = rule;
    }

    /**
     * 获取 enable
     * @return 返回 enable
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * 设置 enable
     * @param 对enable进行赋值
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * 获取 readCount
     * @return 返回 readCount
     */
    public Integer getReadCount() {
        return readCount;
    }

    /**
     * 设置 readCount
     * @param 对readCount进行赋值
     */
    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    /**
     * 获取 favourCount
     * @return 返回 favourCount
     */
    public Integer getFavourCount() {
        return favourCount;
    }

    /**
     * 设置 favourCount
     * @param 对favourCount进行赋值
     */
    public void setFavourCount(Integer favourCount) {
        this.favourCount = favourCount;
    }

    /**
     * 获取 createTime
     * @return 返回 createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置 createTime
     * @param 对createTime进行赋值
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
}
