/*
 * FileName：SysUser.java 
 * <p>
 * Copyright (c) 2017-2020, <a href="http://www.webcsn.com">hermit (794890569@qq.com)</a>.
 * <p>
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl-3.0.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.wxmp.wxcms.domain;

import com.wxmp.core.page.Page;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author hermit
 * @version 2.0
 * @date 2018-04-17 10:54:58
 */
public class SysUser extends Page implements Serializable {
    
	//主键id
	private String id;
	//用户名
	private String account;
	//密码
	private String pwd;
	//密码
	private String email;
	//性别 0男 1女
	private String sex;
	//手机号
	private String phone;
	//姓名
	private String trueName;
	//创建时间
	private String createTime;
	//更新时间
	private String updateTime;
	//状态
	private String flag;
	
	//新登录密码
	private String newpwd;

    /**
     * 获取 id
     * @return 返回 id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置 id
     * @param 对id进行赋值
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取 account
     * @return 返回 account
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置 account
     * @param 对account进行赋值
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取 pwd
     * @return 返回 pwd
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * 设置 pwd
     * @param 对pwd进行赋值
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * 获取 sex
     * @return 返回 sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置 sex
     * @param 对sex进行赋值
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取 phone
     * @return 返回 phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置 phone
     * @param 对phone进行赋值
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取 trueName
     * @return 返回 trueName
     */
    public String getTrueName() {
        return trueName;
    }

    /**
     * 设置 trueName
     * @param 对trueName进行赋值
     */
    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    /**
     * 获取 createTime
     * @return 返回 createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置 createTime
     * @param 对createTime进行赋值
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取 updateTime
     * @return 返回 updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置 updateTime
     * @param 对updateTime进行赋值
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取 flag
     * @return 返回 flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * 设置 flag
     * @param 对flag进行赋值
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * 获取 newpwd
     * @return 返回 newpwd
     */
    public String getNewpwd() {
        return newpwd;
    }

    /**
     * 设置 newpwd
     * @param 对newpwd进行赋值
     */
    public void setNewpwd(String newpwd) {
        this.newpwd = newpwd;
    }

    /**
     * 获取 email
     * @return 返回 email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置 email
     * @param 对email进行赋值
     */
    public void setEmail(String email) {
        this.email = email;
    }
	
	
	
}
