/*
 * FileName：TplMsgText.java 
 * <p>
 * Copyright (c) 2017-2020, <a href="http://www.webcsn.com">hermit (794890569@qq.com)</a>.
 * <p>
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl-3.0.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.wxmp.wxcms.domain;

import lombok.Data;

/**
 *
 * @author hermit
 * @version 2.0
 * @date 2018-04-17 10:54:58
 */
public class TplMsgText extends MsgBase {
	private String title;//消息标题
	private String content;//消息内容
	private Long baseId;//消息主表id
	private String tplId;//消息主表id
	private String wxTpl;//微信模板
    /**
     * 获取 title
     * @return 返回 title
     */
    public String getTitle() {
        return title;
    }
    /**
     * 设置 title
     * @param 对title进行赋值
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * 获取 content
     * @return 返回 content
     */
    public String getContent() {
        return content;
    }
    /**
     * 设置 content
     * @param 对content进行赋值
     */
    public void setContent(String content) {
        this.content = content;
    }
    /**
     * 获取 baseId
     * @return 返回 baseId
     */
    public Long getBaseId() {
        return baseId;
    }
    /**
     * 设置 baseId
     * @param 对baseId进行赋值
     */
    public void setBaseId(Long baseId) {
        this.baseId = baseId;
    }
    /**
     * 获取 tplId
     * @return 返回 tplId
     */
    public String getTplId() {
        return tplId;
    }
    /**
     * 设置 tplId
     * @param 对tplId进行赋值
     */
    public void setTplId(String tplId) {
        this.tplId = tplId;
    }
    /**
     * 获取 wxTpl
     * @return 返回 wxTpl
     */
    public String getWxTpl() {
        return wxTpl;
    }
    /**
     * 设置 wxTpl
     * @param 对wxTpl进行赋值
     */
    public void setWxTpl(String wxTpl) {
        this.wxTpl = wxTpl;
    }
	
	
}
